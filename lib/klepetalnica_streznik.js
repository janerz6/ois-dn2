var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var gesloGledeNaKanal = {};


exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj','-');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function uporabnikiNaKanaluTabela(socket,kanal){
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var uporabniki=[];
  if (uporabnikiNaKanalu.length > 1) {
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      uporabniki.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
    }
  }
  return uporabniki;
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevkiGledeNaSocket[socket.id],
    kanal: vzdevkiGledeNaSocket[socket.id]
  });
  //Osveževanje stanja gostov na danem kanalu in pri uporabniku
  socket.emit('osveziUporabnikeKanala', {
    uporabniki: uporabnikiNaKanaluTabela(socket,trenutniKanal[socket.id])
  });
  socket.broadcast.to(trenutniKanal[socket.id]).emit('osveziUporabnikeKanala', {
    uporabniki: uporabnikiNaKanaluTabela(socket,trenutniKanal[socket.id])
  });
  
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal, geslo) {
 
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  
  socket.emit('pridruzitevOdgovor', {kanal: kanal,vzdevek:vzdevkiGledeNaSocket[socket.id]});
  gesloGledeNaKanal[kanal] = geslo;

  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.',
    uporabnisko: false
  });
  
   //Osveževanje stanja gostov na danem kanalu in pri uporabniku
  socket.emit('osveziUporabnikeKanala', {
    uporabniki: uporabnikiNaKanaluTabela(socket,trenutniKanal[socket.id])
  });
  socket.broadcast.to(trenutniKanal[socket.id]).emit('osveziUporabnikeKanala', {
    uporabniki: uporabnikiNaKanaluTabela(socket,trenutniKanal[socket.id])
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek, uporabnisko: false});
  }
  
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id]
        });
        
        //Osvežimo stanje uporabnikov na kanalu
        socket.emit('osveziUporabnikeKanala', {
          uporabniki: uporabnikiNaKanaluTabela(socket,trenutniKanal[socket.id])
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('osveziUporabnikeKanala', {
          uporabniki: uporabnikiNaKanaluTabela(socket,trenutniKanal[socket.id])
        });
        
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.',
          uporabnisko: false
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo,
      uporabnisko: true
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(podatki) {
      var kanal = podatki.novKanal;
      var geslo = podatki.geslo;
      var kanali = io.sockets.manager.rooms;
      var obstaja=false;
      var prijavi = true;
      var besedilo;
      for (var k in kanali){
        k = k.substring(1, k.length);
        if(k === kanal){
            obstaja=true;
            break;
        }  
      }
      //Če je kanal že postavljen in je zaščiten z geslom
      if(obstaja && gesloGledeNaKanal[kanal] !== "-"){
          if(geslo === gesloGledeNaKanal[kanal]){
             
              
              prijavi=true;
          }
          else{
              besedilo="Pridružitev v kanal "+kanal+" ni bilo uspešno, ker je geslo napačno!";
              socket.emit("pridruzitevNapaka",{sporocilo: besedilo});
              prijavi = false;
          }
      }
      if(obstaja && gesloGledeNaKanal[kanal] === "-" && geslo !== "-"){
            besedilo="Izbrani kanal "+kanal+" je prosto dostopen in ne zahteva prijave z geslom,"+
            "zato se prijavite z uporabo <b>/pridruzitev "+kanal+" </b>ali zahtevajte kreiranje kanala z drugim imenom.";
            socket.emit("pridruzitevNapaka",{sporocilo: besedilo});
            prijavi = false;
         
      }
      
      if (prijavi) {
          socket.leave(trenutniKanal[socket.id]); 
            //Osvežimo stanje zapuščenemu kanalu
            socket.broadcast.to(trenutniKanal[socket.id]).emit('osveziUporabnikeKanala', {
              uporabniki: uporabnikiNaKanaluTabela(socket,trenutniKanal[socket.id])
            });
    
           pridruzitevKanalu(socket, podatki.novKanal,podatki.geslo);
      }  
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}
function obdelajZasebnoSporocilo(socket){
  socket.on('zasebno-sporocilo',function(sporocilo){
    var naslovnikObstaja=false;
    var kanali = io.sockets.manager.rooms;
    var naslovnikID;
    
    for(var kanal in kanali){
        kanal=kanal.substring(1,kanal.length);
      var uporabnikiNaKanalu = io.sockets.clients(kanal); 
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if(sporocilo.naslovnik === vzdevkiGledeNaSocket[uporabnikSocketId]){
              naslovnikObstaja=true;
              naslovnikID=uporabnikSocketId;
             
          }
        }
      }
    }
   
    if(naslovnikObstaja){
     
      
      io.sockets.sockets[naslovnikID].emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] +  '(zasebno): ' + sporocilo.besedilo,
      uporabnisko: true
      });
      socket.emit('sporocilo', {
      besedilo: "(zasebno za "+vzdevkiGledeNaSocket[naslovnikID] + '): ' + sporocilo.besedilo,
      uporabnisko: true
      });
    }
    else{
        
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: "Sporočila "+ sporocilo.besedilo+ " uporabniku z vzdevkom "+ sporocilo.naslovnik + " ni bilo mogoče posredovati."
        }); 
    }
    
  });
}