var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal,geslo) {
 var podatki = {
    novKanal: kanal,
    geslo: geslo
  };
 this.socket.emit('pridruzitevZahteva', podatki);
};

Klepet.prototype.posljiZasebnoSporocilo = function(naslovnik, besedilo) {
  var sporocilo = {
    naslovnik: naslovnik,
    besedilo: besedilo
  };
  this.socket.emit('zasebno-sporocilo', sporocilo);
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var geslo;
      var kanal = besede[0];
      if(kanal.charAt(0) !== "\"" ){
        kanal = besede.join(' ');
        geslo = "-";//Ni gesla
        this.spremeniKanal(kanal,geslo);
      }
      else{
        besede = besede.join(' ');
        if(besede.match(/\"[^\"]{1,}\"/g).length < 2){
          sporocilo = 'Neznan ukaz.';
        }
        else{
          kanal = besede.match(/\"[^\"]{1,}\"/g)[0];
          geslo = besede.match(/\"[^\"]{1,}\"/g)[1];
          kanal = kanal.substring(1,kanal.length-1);
          geslo = geslo.substring(1,geslo.length-1);
          this.spremeniKanal(kanal,geslo);
        }
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      besede = besede.join(' ');
      if(besede.match(/\"[^\"]{1,}\"/g) == null)
      sporocilo = 'Neznan ukaz.';
      else if(besede.match(/\"[^\"]{1,}\"/g).length < 2 ){
        sporocilo = 'Neznan ukaz.';
      }
      else{
        var naslovnik = besede.match(/\"[^\"]{1,}\"/g)[0];
        var sporocilce = besede.match(/\"[^\"]{1,}\"/g)[1];
        naslovnik = naslovnik.substring(1,naslovnik.length-1);
        sporocilce = sporocilce.substring(1,sporocilce.length-1);
        this.posljiZasebnoSporocilo(naslovnik,sporocilce);
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};