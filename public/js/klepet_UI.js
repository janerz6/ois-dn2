var trenutniKanal;
var predpomnilnik={};
var vzdevek;//trenutni vzdevek uporabnika


function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}
function divElementSporciloHtml(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
function dodajSmeske(sporocilo){
  var spremenjenoSporocilo = sporocilo.replace(/</g,"&lt");
  spremenjenoSporocilo = spremenjenoSporocilo.replace(/>/g,"&gt");
  spremenjenoSporocilo= spremenjenoSporocilo.replace(/;\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"></img>');
  spremenjenoSporocilo = spremenjenoSporocilo.replace(/:\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"></img>');
  spremenjenoSporocilo = spremenjenoSporocilo.replace(/\(y\)/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"></img>');
  spremenjenoSporocilo = spremenjenoSporocilo.replace(/:\*/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"></img>');
  spremenjenoSporocilo = spremenjenoSporocilo.replace(/:\(/g,'<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"></img>');

  return spremenjenoSporocilo;
}

function loadXMLDoc(filename)
{
  $.ajax({
		type: "GET",
		url: filename,
		dataType: "xml",
		async: false,
		success: function(xml) {
      predpomnilnik["vulgarizmi"]=xml;
		}
		});
}

function filtrirajVulgarizme(sporocilo,xml){
  $(xml).find("word").each(function(){
        var word = $(this).text();
        var regEx = new RegExp('\\b' + word + '\\b','gi');
        if(sporocilo.search(regEx) != -1){
          var nw="";
          for(var i=0;i<word.length;i++){
            nw+="*";
          }
          sporocilo = sporocilo.replace(regEx,nw);
        }
  });
  return sporocilo;
}

function obdelajSporocilo(sporocilo){
  return dodajSmeske(filterVulgarizmov("swearWords.xml",sporocilo));
}

function filterVulgarizmov(filename,sporocilo){
  var spremenjenoSporocilo;
  if(predpomnilnik["vulgarizmi"]){
    spremenjenoSporocilo = filtrirajVulgarizme(sporocilo,predpomnilnik["vulgarizmi"]);
   
  }
  else{
      loadXMLDoc(filename);
      if(!predpomnilnik["vulgarizmi"]){
        spremenjenoSporocilo="Napaka pri nalaganju...";
      }
      else{
         spremenjenoSporocilo = filtrirajVulgarizme(sporocilo,predpomnilnik["vulgarizmi"]);
      }
  }
  return spremenjenoSporocilo;
}
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementSporciloHtml(obdelajSporocilo(sporocilo)));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';

      $('#sporocila').append(divElementHtmlTekst(sporocilo));
      $('#kanal').text(rezultat.vzdevek + ' @ ' + rezultat.kanal);
      vzdevek=rezultat.vzdevek;
    } else {
      sporocilo = rezultat.sporocilo;
       $('#sporocila').append(divElementHtmlTekst(sporocilo));
    }
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal=rezultat.kanal;
    $('#kanal').text(rezultat.vzdevek + ' @ ' + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  socket.on('pridruzitevNapaka',function(rezultat){
    $('#sporocila').append(divElementHtmlTekst(rezultat.sporocilo));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement;
    if (sporocilo.uporabnisko){
      novElement = $('<div style="font-weight: bold"></div>').html(obdelajSporocilo(sporocilo.besedilo));
    }
    else{
      novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    }
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  socket.on('osveziUporabnikeKanala', function(rezultat){
    $("#uporabniki-kanala").empty();
    if(rezultat.uporabniki.length == 0){
      $("#uporabniki-kanala").append(divElementEnostavniTekst(vzdevek));
    }
    else{
      for(var i=0;i<rezultat.uporabniki.length;i++){
        if(rezultat.uporabniki[i] != vzdevek)
        $("#uporabniki-kanala").append(divElementEnostavniTekst(rezultat.uporabniki[i]));
      }
    }
  });
    
  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});